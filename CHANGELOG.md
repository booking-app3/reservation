# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## 2021-05-08

### Added

- Add reservation model and app skeleton
- Add .gitlab-ci.yml file - test login, build and push for the image to gitlab registry

## 2021-05-09

---

### Added

- Add reservation controller

## 2021-05-19

### Added

- Token based authentication

## 2021-05-23

### Added

- Reservation logic
