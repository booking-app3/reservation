from flask import jsonify, Response, request, json, Blueprint
from sqlalchemy import or_, and_, between
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError
from room import Room
from reservation import Reservation
from utils import required_params, to_date, token_required
from hotel import Hotel
from session import Session
import sys

reservation_controller = Blueprint('reservation_controller', __name__, template_folder='controllers')

@reservation_controller.route("/api/reservations", methods=['GET'])
@required_params({"city_id": int, "check_in": str, "check_out" : str, "nr_pers": int, "stars": int})
@token_required
def get_hotels(guest):
    data = request.get_json(silent=True)
    stars = data['stars']
    check_in = data['check_in']
    check_out = data['check_out']

    session = Session()
    queries = [Hotel.city_id == data['city_id'],
                Room.max_pers >= data['nr_pers'],
                Hotel.stars == stars]

    try:
        to_date(check_in)
        to_date(check_out)
        q = session.query(Reservation) \
            .filter(Reservation.room_id==Room.id,
                or_(
                    between(Reservation.check_in_date, check_in, check_out),
                    between(Reservation.check_out_date, check_in, check_out),
                    and_(
                        check_in <= Reservation.check_in_date,
                        check_out >= Reservation.check_out_date
                        )
                    )
                )
        queries.append(~q.exists())
    except ValueError:
        return Response(status=400)
    try:
        hotels = session.query(Hotel).join(Room).filter(*queries)
    except Exception as e:
        session.rollback()
        return str(e)
    return jsonify(hotels)

@reservation_controller.route("/api/reservations", methods=['POST'])
@required_params({"room_id": int, "check_in": str, "check_out" : str})
@token_required
def add_reservation(guest):

    data = request.get_json(silent=True)
    room_id = data['room_id']
    check_in = data['check_in']
    check_out = data['check_out']
    session = Session()

    try:
        session.query(Room).filter(Room.id == room_id).one()
        q = session.query(Reservation) \
            .filter(Reservation.room_id==room_id,
                or_(
                    between(Reservation.check_in_date, check_in, check_out),
                    between(Reservation.check_out_date, check_in, check_out),
                    and_(
                        check_in <= Reservation.check_in_date,
                        check_out >= Reservation.check_out_date
                        )
                    )
                ).exists()
        if session.query(q).scalar():
            return jsonify({'message' : 'Room is already reserved'}), 402
        reservation = Reservation(room_id=data['room_id'],
                    guest_id=guest.id,
                    check_in_date=to_date(data['check_in']),
                    check_out_date=to_date(data['check_out']))
        session.add(reservation)
        session.commit()

    except IntegrityError:
        session.rollback()
        return Response(status=409)

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        print(e, file=sys.stderr)
        session.rollback()
        return str(e)

    return Response(json.dumps({'id': reservation.id}),
                    status=201,
                    mimetype='application/json')


@reservation_controller.route("/api/reservations/rooms", methods=['POST'])
@required_params({"hotel_id": int, "nr_pers": int, "check_in": str, "check_out" : str})
@token_required
def get_rooms(guest):
    data = request.get_json(silent=True)
    hotel_id = data['hotel_id']
    check_in = data['check_in']
    check_out = data['check_out']
    session = Session()
    queries = [Room.hotel_id == hotel_id,
                Room.max_pers >= data['nr_pers']]
    try:
        session.query(Hotel).filter(Hotel.id == hotel_id).one()
        to_date(check_in)
        to_date(check_out)
        q = session.query(Reservation) \
            .filter(Reservation.room_id==Room.id,
                or_(
                    between(Reservation.check_in_date, check_in, check_out),
                    between(Reservation.check_out_date, check_in, check_out),
                    and_(
                        check_in <= Reservation.check_in_date,
                        check_out >= Reservation.check_out_date
                        )
                    )
                )
        queries.append(~q.exists())
        rooms = session.query(Room).filter(*queries)
    except IntegrityError:
        session.rollback()
        return Response(status=409)

    except NoResultFound:
        session.rollback()
        return Response(status=404)

    except Exception as e:
        print(e, file=sys.stderr)
        session.rollback()
        return str(e)

    return jsonify(rooms)

@reservation_controller.teardown_request
def shutdown_session(exception=None):
    Session.remove()

