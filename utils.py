from flask.json import JSONEncoder
from functools import wraps
from datetime import datetime
from flask import jsonify, request
import jwt
from functools import wraps
from os import environ as env
from guest import Guest
from session import Session
import sys

SECRET_KEY = 'basic-key-login'

class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, datetime):
                return obj.date().isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


def required_params(required):
    def decorator(fn):
        """Decorator that checks for the required parameters"""

        @wraps(fn)
        def wrapper(*args, **kwargs):
            _json = request.get_json()
            missing = [r for r in required.keys()
                       if r not in _json]
            if missing:
                response = {
                    "status": "error",
                    "message": "Request JSON is missing some required params",
                    "missing": missing
                }
                return jsonify(response), 400
            wrong_types = [r for r in required.keys()
                           if not isinstance(_json[r], required[r])]
            if wrong_types:
                response = {
                    "status": "error",
                    "message": "Data types in the request JSON doesn't match the required format",
                    "param_types": {k: str(v) for k, v in required.items()}
                }
                return jsonify(response), 400
            return fn(*args, **kwargs)

        return wrapper

    return decorator


def to_date(date_string):
    return datetime.strptime(date_string, "%Y-%m-%d").date()

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        session = Session()
        token = None
        # jwt is passed in the request header
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
        # return 401 if token is not passed
        if not token:
            return jsonify({'message' : 'Token is missing !!'}), 401
        try:
            data = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
            current_user = session.query(Guest).filter(Guest.id == data['public_id']).one()
        except Exception as e:
            session.rollback()
            Session.remove()
            return jsonify({
                'message' : f'Token is invalid!!'
            }), 401
        Session.remove()
        return  f(current_user, *args, **kwargs)

    return decorated

