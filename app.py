from flask import Flask
from reservation_controller import reservation_controller
from utils import CustomJSONEncoder

app = Flask(__name__)
app.json_encoder = CustomJSONEncoder
app.register_blueprint(reservation_controller)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug = True, port=4321)