from base import Base
from dataclasses import dataclass
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship


@dataclass
class Room(Base):
    __tablename__ = 'Room'

    id: int
    room_floor: int
    hotel_id: int
    max_pers: int
    description: str

    id = Column(Integer, primary_key=True, autoincrement=True)
    room_floor = Column(Integer)
    hotel_id = Column(Integer, ForeignKey("Hotel.id", ondelete="CASCADE"))
    max_pers = Column(Integer)
    description = Column(String)
    hotel = relationship('Hotel', back_populates="rooms")