from base import Base
from dataclasses import dataclass
from room import Room
from comfort_level import ComfortLevel
from sqlalchemy import Column, Integer, String, ForeignKey, Table
from sqlalchemy.orm import relationship
from city import City

@dataclass
class Hotel(Base):
    __tablename__ = 'Hotel'

    id: int
    hotel_name: str
    stars: int
    city_id: int

    id = Column(Integer, primary_key=True, autoincrement=True)
    hotel_name = Column(String)
    stars = Column(Integer)
    city_id = Column(Integer, ForeignKey(City.id, ondelete="CASCADE"))
    city = relationship(City, back_populates="hotels")
    rooms = relationship(Room, back_populates="hotel", cascade="all")
    comfort_levels = relationship(ComfortLevel,
                                  secondary=lambda: association_table)
    def __init__(self, hotel_name, stars, city_id):
        self.hotel_name = hotel_name
        self.stars = stars
        self.city_id = city_id

association_table = Table('HotelComfortLevel', Base.metadata,
                          Column('comf_level_id', Integer, ForeignKey('ComfortLevel.id')),
                          Column('hotel_id', Integer, ForeignKey('Hotel.id'))
                          )
