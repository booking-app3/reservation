from base import Base
from dataclasses import dataclass
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship


@dataclass
class City(Base):
    __tablename__ = 'City'

    id: int
    city_name: str

    id = Column(Integer, primary_key=True, autoincrement=True)
    city_name = Column(String)
    hotels = relationship('Hotel', back_populates="city", cascade="all")
