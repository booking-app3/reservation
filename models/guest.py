from base import Base
from dataclasses import dataclass
from sqlalchemy import Column, Integer, String
from sqlalchemy import UniqueConstraint


@dataclass
class Guest(Base):
    __tablename__ = 'Guest'

    id: int
    username: str
    password: str
    email: str

    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String)
    password = Column(String)
    email = Column(String)

    UniqueConstraint(username)
    UniqueConstraint(email)
