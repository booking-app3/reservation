from base import Base
from dataclasses import dataclass
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from datetime import datetime


@dataclass
class Reservation(Base):
    __tablename__ = 'Reservation'

    id: int
    room_id: int
    guest_id: int
    check_in_date: datetime
    check_out_date: datetime

    id = Column(Integer, primary_key=True, autoincrement=True)
    check_in_date = Column(DateTime())
    check_out_date = Column(DateTime())
    room_id = Column(Integer, ForeignKey("Room.id", ondelete="CASCADE"))
    guest_id = Column(Integer, ForeignKey("Guest.id", ondelete="CASCADE"))

